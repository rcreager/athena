#include "../AthenaPoolAddressProviderSvc.h"
#include "../EventSelectorAthenaPool.h"
#include "../DoubleEventSelectorAthenaPool.h"
#include "../StreamSelectorTool.h"
#include "../CondProxyProvider.h"

DECLARE_COMPONENT( AthenaPoolAddressProviderSvc )
DECLARE_COMPONENT( EventSelectorAthenaPool )
DECLARE_COMPONENT( DoubleEventSelectorAthenaPool )
DECLARE_COMPONENT( StreamSelectorTool )
DECLARE_COMPONENT( CondProxyProvider )

